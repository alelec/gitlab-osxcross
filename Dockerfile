ARG OSXCROSS_VERSION=13.1-r0
FROM --platform=$BUILDPLATFORM crazymax/osxcross:${OSXCROSS_VERSION}-ubuntu AS osxcross

FROM ubuntu:noble-20240429
RUN apt-get update && apt-get install -y clang lld libc6-dev make file
ENV PATH="/osxcross/bin:$PATH"
ENV LD_LIBRARY_PATH="/osxcross/lib:$LD_LIBRARY_PATH"

COPY --from=osxcross /osxcross /osxcross
